VERSION=0.16.2

download:
	curl -o docker-compose.yml https://raw.githubusercontent.com/operable/cog/${VERSION}/docker-compose.yml
	curl -o docker-compose.common.yml https://raw.githubusercontent.com/operable/cog/${VERSION}/docker-compose.common.yml
	curl -o docker-compose.override.yml https://raw.githubusercontent.com/operable/cog/${VERSION}/docker-compose.override.slack_example.yml
