#!/bin/bash
source vars.sh

sed -i "/export\ SLACK_API_TOKEN=${SLACK_API_TOKEN}/d" /home/user/.bashrc
sed -i "/export\ COG_HOST=${HOST_IP}/d" /home/user/.bashrc
